import React from 'react'
import { Text, Button} from 'react-native'
import { Container, Content } from '../components/Styled'
function Detail({navigation}) {
  return (
    <Container>
      <Content>
        <Text>Detail</Text>
        <Button title="List" onPress={() => {navigation.goBack()}}></Button>
      </Content>
    </Container>
  )
}

export default Detail
