import React from 'react'
import { Text} from 'react-native'
import { Container, Content, Button} from '../components/Styled'

// 구조 분해 할당, Destructuring Assignment
function Form({navigation}) {
  return (
    <Container>
      <Content>
        <Text>Form</Text>
      </Content>
      <Button onPress={()=>{navigation.goBack()}}>완료</Button>
    </Container>
  )
}

export default Form
