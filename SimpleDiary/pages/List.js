import React from 'react'
import { Text } from 'react-native'
import { Container, Content, Button } from '../components/Styled'

function List({navigation}) {
  return (
    <Container>
      <Content>
        <Text>List</Text>
      </Content>
      <Button onPress={()=>navigation.navigate('Form')}>새 일기 쓰기</Button>
    </Container>
  )
}

export default List
