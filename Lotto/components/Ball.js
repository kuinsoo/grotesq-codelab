import styled from 'styled-components/native'

//로또 공 생성
const Ball = styled.View`
  width: 50px;
  height: 50px;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
  background: ${props => {
    if(props.value<11) {
      return '#e3e016';
    } else if(props.value<21) {
      return '#5bf15b';
    } else if(props.value<31) {
      return '#e588f1';
    } else if(props.value<41) {
      return '#eca56f';
    }else {
      return '#878482';
    }
  }};
`
export default Ball
