import styled from 'styled-components/native'

const Label = styled.Text`
font-size: 20px;
font-weight: bold;
color: #000000;
`

export default Label