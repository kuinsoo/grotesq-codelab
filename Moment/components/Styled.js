// 리액트 네이티브에서는 /native 추가
import styled from 'styled-components/native'

const Container = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
`

const Row = styled.View`
  flex-direction: row;
`

const Label = styled.Text`
  font-size: 36px;
  font-weight: bold;
`

export {Container, Row, Label}

