import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Platform } from 'react-native';
import { Container, Contents, InputContainer, Input, 
          Button, TodoItem, TodoItemText, TodoItemButton,
          KeyboardAvoidingView,
          TempText, Check, CheckIcon
      } from './components/Styled'
import _ from 'lodash'
import AsyncStorage from '@react-native-community/async-storage'
import produce from 'immer'

/* AsyncStorage 가져오기 */
// AsyncStorage.getItem('test')
// .then(data=>{
//   alert(data)
// }).catch(error=>{
//   alert(error.message)
// });
/* AsyncStorage 저장 */
// AsyncStorage.setItem('test', 'test value')
// .then(()=>{
//   alert('저장됨')
// }).catch(error=>{
//   alert(error.message)
// });

export default function App() {
  const [list, setList] = React.useState([])

  const [inputTodo, setInputTodo] = React.useState('할 일 입력')

  React.useEffect(() => {
    AsyncStorage.getItem('list')
    .then(data=>{
      if(data !== null){
        setList(JSON.parse(data))
      }
    }).catch(error=>{
      alert(error.message)
    })
    return () => {
    }
  }, [])
  
  const store = (newList)=>{
    setList(newList)
    AsyncStorage.setItem('list', JSON.stringify(newList))
  }
  // 리턴 가능 컴포넌트, 컴포넌트로 이루어진 배열
  return (
    <Container>
      <StatusBar></StatusBar>
        <KeyboardAvoidingView 
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        >

          <Contents>
            {list.map( item => {
              return (
                <TodoItem key={item.id}>
                  <Check onPress={()=>{store(produce(list, draft => {
                    const index = list.indexOf(item)
                    draft[index].done = !item.done
                  }))}}><CheckIcon>{item.done ? '✅' : '☑️'}</CheckIcon></Check>
                  <TodoItemText>{item.todo}</TodoItemText>
                  <TodoItemButton title="삭제" onPress={()=>{
                    // const rejectedList = _.reject(list, element => element.id === item.id)
                    // setList( rejectedList )
                    store(_.reject(list, element => element.id === item.id))
                  }} />
                </TodoItem>
              )
            })}
          </Contents>

          <InputContainer>
            <Input placeholder="할일을 입력해주세요" placeholderTextColor="blue" 
                value={inputTodo} onChangeText={ value => setInputTodo(value)} />
            <Button title="전송" onPress={() => {
              if(inputTodo === '') {
                return ;
              }
              const newItem = {
                id: new Date().getTime().toString(),
                todo: inputTodo,
                done: false,
              }
              store( [
                ...list, // 전개 연산자 Speread Operator
                newItem,
              ])
              setInputTodo('')
              
            }}/>
          </InputContainer>

        </KeyboardAvoidingView>
    </Container>
  );
}
