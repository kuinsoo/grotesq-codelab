import styled from 'styled-components/native'

const Container = styled.SafeAreaView`
  flex: 1;
`
const Contents = styled.ScrollView`
  flex: 1;
  padding: 8px 24px;
`
const InputContainer = styled.View`
 flex-direction: row; 
 padding: 8px 24px;
`
const Input = styled.TextInput`
  border: 1px solid #e5e5e5e5;
  flex: 1;
`
const TodoItem = styled.View`flex-direction: row; align-items:center;`
const TodoItemText = styled.Text`font-size: 20px; flex: 1;`
const TodoItemButton = styled.Button``
const Button = styled.Button`
  
`
const KeyboardAvoidingView = styled.KeyboardAvoidingView`
  flex: 1;
`
const TempText = styled.Text`
  font-size: 20px;
  margin-bottom: 12px;
`

const Check = styled.TouchableOpacity`
  margin-right: 4px;
`;

const CheckIcon = styled.Text`
  font-size: 20px;
`;
export {Container, Contents, InputContainer, Input,
        Button, TodoItem, TodoItemText, TodoItemButton, TempText,
        KeyboardAvoidingView, Check, CheckIcon,
      }